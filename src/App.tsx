import './index.css'
import './reset.css'

import { useMemo } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

import { AppBar } from './components/atoms/AppBar'
import { LoadingIndicator } from './components/atoms/LoadingIndicator'
import { SuperHeroProvider } from './contexts/superHeroContext/SuperHeroProvider'
import { HomePage } from './pages/home/HomePage'
import { SuperHeroPage } from './pages/superhero/SuperHeroPage'

function App() {
  const basename = useMemo(() => {
    if (process.env.PUBLIC_URL.includes('gitlab')) return '/info-case'

    return ''
  }, [process.env.PUBLIC_URL])

  return (
    <BrowserRouter basename={basename}>
      <SuperHeroProvider>
        <AppBar />
        <LoadingIndicator />
        <main>
          <Routes>
            <Route path='/' element={<HomePage />} />
            <Route path='/superhero/:id' element={<SuperHeroPage />} />
          </Routes>
        </main>
      </SuperHeroProvider>
    </BrowserRouter>
  )
}

export default App
