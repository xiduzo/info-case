export type Thunk = (dispatch: React.Dispatch<unknown>) => Promise<void>
