export enum LocalStorageKey {
  SuperHeroes = 'superheroes',
  Comics = 'comics',
  Offset = 'offset',
}
