export interface Comic {
  id: number
  title: string
  description: string
  /**
   * A string array of creators surnames
   */
  creators: string[]
  /**
   * An array of superHero names participating in this comic
   */
  superHeroes: string[]
  imageUri: string
}
