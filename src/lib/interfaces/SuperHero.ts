export interface SuperHero {
  id: number
  name: string
  imageUri: string
  comics: {
    available: number
    uri: string
  }
}
