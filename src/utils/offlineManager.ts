interface LocalStorageItem {
  id: number
}

export interface OfflineEvent {
  id: string
  name: string
  args: any[]
}

/**
 * Check if the user has an active internet connection.
 */
export const isOnline = (): boolean => navigator.onLine

/**
 * Get an item stored in the offline store.
 * @param key The key with which the item is saved
 * @param fallback What will be returned when no value has been found. Defaults to `new Object()`
 * @param id Get the item by id. This will be ignored when the stored data is not an array
 */
export const getLocalItem = <T>(
  key: string,
  fallback: T = new Object() as T,
  id?: number | string
): T => {
  const localItem = localStorage.getItem(key)

  if (!localItem) return fallback

  let item: any

  try {
    item = JSON.parse(localItem)
  } catch {
    item = localItem
  }

  if (!id) return item ?? fallback

  if (Array.isArray(item)) {
    const parsedId = parseInt(id as string, 10)
    return item.find((i: Partial<LocalStorageItem>): boolean => i.id === parsedId)
  }

  if (item !== null && typeof item === 'object') return item.get(id)

  return item ?? fallback
}

/**
 * Store an item in the offline store.
 * @param key The key with which the item is saved
 * @param item Item to be stored
 */

export const setLocalItem = <T>(key: string, item: T): T => {
  try {
    localStorage.setItem(key, JSON.stringify(item))
  } catch (error) {
    console.error('local storage not supported')
  }
  return item
}

/**
 * Add an item to the offline store.
 * @param key The key with which the item is saved
 * @param item Item to be stored
 */

export const addLocalItem = <T>(key: string, item: T): T => {
  let localItem: any = getLocalItem<T>(key)

  localItem = Array.isArray(localItem) ? [...localItem, item] : item

  setLocalItem<T>(key, localItem)

  return item
}

/**
 * Update an item in the offline store.
 * @param key The key with which the item is saved
 * @param item Item to be stored
 * @param id Update a item by id. This will be ignored when the stored data is not an array
 */

export const updateLocalItem = <T>(key: string, item: T, id?: string | number): T => {
  let localItem: any = getLocalItem<T>(key)

  localItem =
    id && Array.isArray(localItem)
      ? localItem.map((i: Partial<LocalStorageItem>): Partial<LocalStorageItem> => {
          return i.id === parseInt(id as string, 10) ? item : i
        })
      : item

  setLocalItem<T>(key, localItem)

  return item
}

/**
 * Remove item from the offline store.
 * @param key The key with which the item is saved
 * @param id Get a item by id. This will be ignored when the stored data is not an array
 */
export const removeLocalItem = <T>(key: string, id?: number | string): T | void => {
  let item: any = getLocalItem<T>(key)

  if (id && Array.isArray(item)) {
    item = item.filter((i: Partial<LocalStorageItem>): boolean => i.id !== id)
    return setLocalItem<T>(key, item)
  }

  return localStorage.removeItem(key)
}

/**
 * Add an event to be processed when the user will be back online.
 * @param name Name of the event, preferably the name of the function to be called
 * @param args Any arguments which the function needs to be executed with
 */
export const addOfflineEvent = (name: string, ...args: any): void => {
  const event: OfflineEvent = {
    id: Date.now().toString(),
    name,
    args: [...args],
  }

  addLocalItem<OfflineEvent>('offlineEvents', event)
}
