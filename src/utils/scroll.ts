// https://stackoverflow.com/a/4770179
// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
const scrollKeys: Record<number, boolean> = {
  32: true,
  33: true,
  34: true,
  35: true,
  36: true,
  37: true,
  38: true,
  39: true,
  40: true,
}

const preventDefault = (e: Event): void => e.preventDefault()

const preventDefaultForScrollKeys = (e: Event & { keyCode: number }): void => {
  if (scrollKeys[e.keyCode]) {
    preventDefault(e)
    return
  }
}

// modern Chrome requires { passive: false } when adding event
var supportsPassive = false
try {
  window.addEventListener(
    'click',
    {} as EventListenerOrEventListenerObject,
    Object.defineProperty({}, 'passive', {
      get: function () {
        supportsPassive = true
        return supportsPassive
      },
    })
  )
} catch (e) {}

var wheelOpt = supportsPassive ? { passive: false } : false
var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel'

// call this to Disable
export const disableScroll = (): void => {
  window.addEventListener('DOMMouseScroll', preventDefault, false) // older FF
  window.addEventListener(wheelEvent, preventDefault, wheelOpt) // modern desktop
  window.addEventListener('touchmove', preventDefault, wheelOpt) // mobile
  window.addEventListener('keydown', preventDefaultForScrollKeys, false)
}

// TODO: add correct typings for removeEventListener
export const enabledScroll = (): void => {
  window.removeEventListener('DOMMouseScroll', preventDefault, false)
  // @ts-ignore
  window.removeEventListener(wheelEvent, preventDefault, wheelOpt)
  // @ts-ignore
  window.removeEventListener('touchmove', preventDefault, wheelOpt)
  window.removeEventListener('keydown', preventDefaultForScrollKeys, false)
}
