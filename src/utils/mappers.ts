import { Comic } from '../lib/interfaces/Comic'
import { SuperHero } from '../lib/interfaces/SuperHero'

export const superHeroMapper = (superHero: any): SuperHero => {
  const mappedHero = {
    id: superHero.id,
    name: superHero.name,
    imageUri: superHero.thumbnail.path + '.' + superHero.thumbnail.extension,
    comics: {
      available: superHero.comics.available,
      uri: superHero.comics.collectionURI,
    },
  }

  return mappedHero
}

export const comicMapper = (comic: any): Comic => {
  const mappedComic = {
    id: comic.id,
    title: comic.title,
    description:
      comic.description && comic.description !== ''
        ? comic.description
        : comic.variantDescription,
    creators: comic.creators.items.map((creator: any) => {
      const names = creator.name.split(' ')
      return names[names.length - 1] // Poor mans creator surname
    }),
    superHeroes: comic.characters.items.map((character: any) => character.name),
    imageUri: comic.thumbnail.path + '.' + comic.thumbnail.extension,
  }

  return mappedComic
}
