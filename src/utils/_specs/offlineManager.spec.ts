import { addLocalItem, getLocalItem, removeLocalItem, setLocalItem } from '../offlineManager'

describe('OfflineManager', () => {
  beforeEach(() => {
    localStorage.clear()
  })

  it('Should set and get a local item', () => {
    const toStore = 'bar'
    setLocalItem('foo', 'bar')

    const result = getLocalItem('foo')

    expect(result).toBe(toStore)
  })

  it('Should be able to store objects', () => {
    const toStore = { bar: 'baz' }
    setLocalItem('foo', toStore)

    const result = getLocalItem('foo')

    expect(result).toStrictEqual(toStore)
  })

  it('Should be able to store complex objects', () => {
    const toStore = { bar: 'baz', array: [1, 2, 3, 4], object: { foo: 'bar' } }
    setLocalItem('foo', toStore)

    const result = getLocalItem('foo')

    expect(result).toStrictEqual(toStore)
  })

  it('Should be able to add an item to an array', () => {
    const toStore = 'baz'

    setLocalItem('foo', ['bar'])
    addLocalItem('foo', toStore)

    const result = getLocalItem('foo')

    expect(result).toStrictEqual(['bar', 'baz'])
  })

  it('Should set localItem while adding to non existing key', () => {
    const toStore = 'foo'

    addLocalItem('foo', toStore)

    const result = getLocalItem('foo')

    expect(result).toStrictEqual(toStore)
  })

  it('Should return the fallback value when key does not exist', () => {
    const result = getLocalItem('foo')

    expect(result).toStrictEqual({})
  })

  it('Should be able to overwrite the fallback value', () => {
    const result = getLocalItem('foo', 'bar')

    expect(result).toStrictEqual('bar')
  })

  it('Should be able to remove a stored item', () => {
    const toStore = 'baz'

    setLocalItem('foo', toStore)
    removeLocalItem('foo')

    const result = getLocalItem('foo')

    expect(result).toStrictEqual({})
  })
})
