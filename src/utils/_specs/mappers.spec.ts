import { comicMapper, superHeroMapper } from '../mappers'

describe('superHeroMapper', () => {
  //#region baseSuperHeroResponse
  const baseSuperHeroResponse = {
    id: 1017438,
    name: 'Araٌa',
    description: '',
    modified: '2016-03-07T14:41:37-0500',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg',
    },
    resourceURI: 'http://gateway.marvel.com/v1/public/characters/1017438',
    comics: {
      available: 22,
      collectionURI: 'http://gateway.marvel.com/v1/public/characters/1017438/comics',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/21003',
          name: 'Amazing Spider-Girl (2006) #19',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/22903',
          name: 'Amazing Spider-Girl (2006) #27',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/30311',
          name: 'Amazing Spider-Man (1999) #636',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/30312',
          name: 'Amazing Spider-Man (1999) #637',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/2311',
          name: 'ARANA VOL. 2: IN THE BEGINNING DIGEST (Digest)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/3497',
          name: 'ARANA VOL. 3: NIGHT OF THE HUNTER DIGEST (Digest)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/1589',
          name: 'Arana: The Heart of the Spider (2005) #1',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/1679',
          name: 'Arana: The Heart of the Spider (2005) #2',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/1778',
          name: 'Arana: The Heart of the Spider (2005) #3',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/1881',
          name: 'Arana: The Heart of the Spider (2005) #4',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/1983',
          name: 'Arana: The Heart of the Spider (2005) #5',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/2086',
          name: 'Arana: The Heart of the Spider (2005) #6',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/2258',
          name: 'Arana: The Heart of the Spider (2005) #7',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/2364',
          name: 'Arana: The Heart of the Spider (2005) #8',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/2530',
          name: 'Arana: The Heart of the Spider (2005) #9',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/2531',
          name: 'Arana: The Heart of the Spider (2005) #10',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/3066',
          name: 'Arana: The Heart of the Spider (2005) #11',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/3165',
          name: 'Arana: The Heart of the Spider (2005) #12',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/37044',
          name: 'Avengers Academy Giant-Size (2010) #1',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/67950',
          name: 'Captain Marvel: Carol Danvers - The Ms. Marvel Years Vol. 2 (Trade Paperback)',
        },
      ],
      returned: 20,
    },
    series: {
      available: 8,
      collectionURI: 'http://gateway.marvel.com/v1/public/characters/1017438/series',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/1126',
          name: 'Amazing Spider-Girl (2006 - 2009)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/454',
          name: 'Amazing Spider-Man (1999 - 2013)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/1454',
          name: 'ARANA VOL. 2: IN THE BEGINNING DIGEST (2005)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/1562',
          name: 'ARANA VOL. 3: NIGHT OF THE HUNTER DIGEST (2006)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/853',
          name: 'Arana: The Heart of the Spider (2005)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/13250',
          name: 'Avengers Academy Giant-Size (2010)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/24528',
          name: 'Captain Marvel: Carol Danvers - The Ms. Marvel Years Vol. 2 (2018)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/813',
          name: 'Marvel Team-Up (2004 - 2006)',
        },
      ],
      returned: 8,
    },
    stories: {
      available: 20,
      collectionURI: 'http://gateway.marvel.com/v1/public/characters/1017438/stories',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4131',
          name: '1 of 3',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4133',
          name: '2 of 4 - League of Losers',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4436',
          name: '1 of 6 - Freshman Flu',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4438',
          name: '2 of 6 - Freshman Flu',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4440',
          name: '3 of 6 - In the Beginning',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4444',
          name: '4 of 6 - In the Beginning',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4446',
          name: '5 of 6 - In the Beginning',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4448',
          name: '6 of 6 - In the Beginning',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4450',
          name: '1 of 6',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4452',
          name: '2 of 6',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4454',
          name: '3 of 6',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4456',
          name: '4 of 6',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4458',
          name: '5 of 6',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/4460',
          name: '6 of 6',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/45556',
          name: 'Spider-Girl vs. Arana 1 of 1',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/50852',
          name: '2 of 6',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/69448',
          name: 'Amazing Spider-Man (1999) #636',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/69450',
          name: 'Amazing Spider-Man (1999) #637',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/82123',
          name: 'Avengers Academy Giant-Size #1',
          type: 'cover',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/stories/147544',
          name: 'cover from CAPTAIN MARVEL: CAROL DANVERS - THE MS. MARVEL YEARS VOL. 2 TPB (2018) #2',
          type: 'cover',
        },
      ],
      returned: 20,
    },
    events: {
      available: 0,
      collectionURI: 'http://gateway.marvel.com/v1/public/characters/1017438/events',
      items: [],
      returned: 0,
    },
    urls: [
      {
        type: 'detail',
        url: 'http://marvel.com/characters/176/araa?utm_campaign=apiRef&utm_source=073ee8b244f3f362e294426f2edf499e',
      },
      {
        type: 'comiclink',
        url: 'http://marvel.com/comics/characters/1017438/araa?utm_campaign=apiRef&utm_source=073ee8b244f3f362e294426f2edf499e',
      },
    ],
  }
  //#endregion baseSuperHeroResponse

  it('will map an API response to a super hero object', () => {
    /**
     * Arrange
     */

    /**
     * Act
     */
    const superHero = superHeroMapper(baseSuperHeroResponse)

    /**
     * Assert
     */
    expect(superHero).toBeDefined()
    expect(superHero.id).toBe(1017438)
    expect(superHero.name).toBe('Araٌa')
    expect(superHero.imageUri).toBe(
      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg'
    )
    expect(superHero.comics).toStrictEqual({
      available: 22,
      uri: 'http://gateway.marvel.com/v1/public/characters/1017438/comics',
    })
  })
})

describe('comicMapper', () => {
  //#region baseComicResponse
  const baseComicResponse = {
    id: 5425,
    digitalId: 0,
    title: 'Squadron Supreme Vol. 1: The Pre-War Years Premiere (Hardcover)',
    issueNumber: 0,
    variantDescription: '',
    description:
      "It all starts here! Hyperion, Nighthawk, Blur, Power Princess, Doctor Spectrum and the rest of the deadliest super-team around are joined by a new group of super heroes, as only J. Michael Straczynski and Gary Frank can imagine! As the U.S. government plots to create two teams of super-powered agents to crush enemies both domestic and foreign, Mark Milton - a.k.a. Hyperion - has plans of his own. Plans that could uproot the government's control over its super-powered population and force them to contend with the ever-growing threat of Mark's constant insubordination. Collects SQUADRON SUPREME #1-7.\r<br>168 PGS./Parental Advisory ...$22.99\r<br>",
    modified: '2016-02-09T13:07:30-0500',
    isbn: '0-7851-2282-6',
    upc: '5960612282-00111',
    diamondCode: '',
    ean: '',
    issn: '',
    format: 'Hardcover',
    pageCount: 0,
    textObjects: [
      {
        type: 'issue_solicit_text',
        language: 'en-us',
        text: "It all starts here! Hyperion, Nighthawk, Blur, Power Princess, Doctor Spectrum and the rest of the deadliest super-team around are joined by a new group of super heroes, as only J. Michael Straczynski and Gary Frank can imagine! As the U.S. government plots to create two teams of super-powered agents to crush enemies both domestic and foreign, Mark Milton - a.k.a. Hyperion - has plans of his own. Plans that could uproot the government's control over its super-powered population and force them to contend with the ever-growing threat of Mark's constant insubordination. Collects SQUADRON SUPREME #1-7.\r<br>168 PGS./Parental Advisory ...$22.99\r<br>",
      },
    ],
    resourceURI: 'http://gateway.marvel.com/v1/public/comics/5425',
    urls: [
      {
        type: 'detail',
        url: 'http://marvel.com/comics/collection/5425/squadron_supreme_vol_1_the_pre-war_years_premiere_hardcover?utm_campaign=apiRef&utm_source=073ee8b244f3f362e294426f2edf499e',
      },
    ],
    series: {
      resourceURI: 'http://gateway.marvel.com/v1/public/series/1791',
      name: 'Squadron Supreme Vol. 1: The Pre-War Years Premiere (2006)',
    },
    variants: [],
    collections: [],
    collectedIssues: [
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/4419',
        name: 'Squadron Supreme (2006) #5',
      },
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/4260',
        name: 'Squadron Supreme (2006) #4',
      },
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/4150',
        name: 'Squadron Supreme (2006) #3',
      },
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/4038',
        name: 'Squadron Supreme (2006) #2',
      },
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/3908',
        name: 'Squadron Supreme (2006) #1',
      },
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/3568',
        name: 'Saga of the Squadron Supreme (2006) #1',
      },
    ],
    dates: [
      { type: 'onsaleDate', date: '2006-11-15T00:00:00-0500' },
      { type: 'focDate', date: '-0001-11-30T00:00:00-0500' },
    ],
    prices: [{ type: 'printPrice', price: 9.99 }],
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/60/4bc5feb26c55e',
      extension: 'jpg',
    },
    images: [
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/c0/56ba2ad43e61c',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/7/e0/56ba2aa04b221',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/20/56ba2a345d394',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/a0/56ba2a07f1ec2',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/50/56ba29ccb3718',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/60/4bc5feb26c55e',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/2/20/4bc5fd16dca6d',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/60/4bc5f403f3494',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/70/4bc5ebb59f36b',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/30/4bc5e44557231',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/2/10/4bc5dbfcae229',
        extension: 'jpg',
      },
      {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/20/4bc5bc9660630',
        extension: 'jpg',
      },
    ],
    creators: {
      available: 4,
      collectionURI: 'http://gateway.marvel.com/v1/public/comics/5425/creators',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/creators/308',
          name: 'Gary Frank',
          role: 'penciller (cover)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/creators/8429',
          name: 'Dave Sharpe',
          role: 'letterer',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/creators/433',
          name: 'Jonathan Sibal',
          role: 'inker',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/creators/500',
          name: 'Christopher Sotomayor',
          role: 'colorist',
        },
      ],
      returned: 4,
    },
    characters: {
      available: 11,
      collectionURI: 'http://gateway.marvel.com/v1/public/comics/5425/characters',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010673',
          name: 'Amphibian (Earth-712)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010686',
          name: 'Arcana',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010688',
          name: 'Blur',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010689',
          name: 'Doctor Spectrum',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009361',
          name: 'Hyperion (Earth-712)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1011064',
          name: 'Inertia',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010349',
          name: 'Nighthawk',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010707',
          name: 'Nuke',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010712',
          name: 'Shape',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010814',
          name: 'Squadron Supreme (Earth-712)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010716',
          name: 'Zarda',
        },
      ],
      returned: 11,
    },
  }
  //#endregion baseComicResponse

  it('will map an API response to a comic object', () => {
    /**
     * Arrange
     */

    /**
     * Act
     */
    const comic = comicMapper(baseComicResponse)

    /**
     * Assert
     */
    expect(comic).toBeDefined()
    expect(comic.id).toBe(5425)
    expect(comic.title).toBe(
      'Squadron Supreme Vol. 1: The Pre-War Years Premiere (Hardcover)'
    )
    expect(comic.description).toBe(
      "It all starts here! Hyperion, Nighthawk, Blur, Power Princess, Doctor Spectrum and the rest of the deadliest super-team around are joined by a new group of super heroes, as only J. Michael Straczynski and Gary Frank can imagine! As the U.S. government plots to create two teams of super-powered agents to crush enemies both domestic and foreign, Mark Milton - a.k.a. Hyperion - has plans of his own. Plans that could uproot the government's control over its super-powered population and force them to contend with the ever-growing threat of Mark's constant insubordination. Collects SQUADRON SUPREME #1-7.\r<br>168 PGS./Parental Advisory ...$22.99\r<br>"
    )
    expect(comic.creators).toStrictEqual(['Frank', 'Sharpe', 'Sibal', 'Sotomayor'])
  })

  it('will fallback to the variantDescription when no description has been provided', () => {
    /**
     * Arrange
     */
    const response = {
      ...baseComicResponse,
      description: undefined,
      variantDescription: 'A description',
    }

    /**
     * Act
     */
    const comic = comicMapper(response)

    /**
     * Assert
     */
    expect(comic.description).toBe('A description')
  })

  it('will fallback to the variantDescription when the description is ""', () => {
    /**
     * Arrange
     */
    const response = {
      ...baseComicResponse,
      description: '',
      variantDescription: 'A description',
    }

    /**
     * Act
     */
    const comic = comicMapper(response)

    /**
     * Assert
     */
    expect(comic.description).toBe('A description')
  })
})
