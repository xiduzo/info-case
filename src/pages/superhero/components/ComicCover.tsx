import { FC } from 'react'
import styled from 'styled-components'

interface ComicCoverProps extends React.HTMLAttributes<HTMLDivElement> {
  imageUri: string
  title: string
}

const StyledComicCover = styled.div.attrs((props: ComicCoverProps) => ({}))`
  width: 100%;

  & > div {
    background: var(--color-dark);
    position: relative;
    height: 650px;

    @media (min-width: 768px) {
      height: 500px;
    }

    @media (min-width: 1024px) {
      height: 450px;
    }

    @media (min-width: 1201px) {
      height: 370px;
    }
  }

  img {
    max-width: 100%;
    min-width: 100%;
    height: 100%;
    transition: all 0.2s ease-in-out;

    &:nth-child(2) {
      left: 0;
      position: absolute;
      transform: scale(1.01);
      filter: blur(8px);
      opacity: 0.6;
      z-index: -1;
    }
  }
`

export const ComicCover: FC<ComicCoverProps> = props => {
  /*
   * State
   */
  const { imageUri, title } = props

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  return (
    <StyledComicCover {...props}>
      <div>
        <img src={imageUri} alt={title} />
        <img src={imageUri} alt={title} />
      </div>
    </StyledComicCover>
  )
}
