import { FC, useMemo } from 'react'
import styled from 'styled-components'

import { ButtonBase } from '../../../components/atoms/buttons/ButtonsBase'
import { Typography } from '../../../components/atoms/Typography'
import { Comic } from '../../../lib/interfaces/Comic'
import { ComicCover } from './ComicCover'

interface ComicCardProps extends React.HTMLAttributes<HTMLDivElement> {
  creators: string[]
  title: string
  imageUri: string
}

const StyledComicCard = styled(ButtonBase).attrs((props: ComicCardProps) => ({}))``

export const ComicCard: FC<ComicCardProps> = props => {
  /*
   * State
   */
  const { creators, title, imageUri } = props

  const joinedCreators = useMemo(() => creators.join(', '), [creators])

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  return (
    <StyledComicCard {...props}>
      <ComicCover imageUri={imageUri} title={title} style={{ marginBottom: '2rem' }} />
      <Typography variant='h4' gutterBottom>
        {title}
      </Typography>
      <Typography>{joinedCreators}</Typography>
    </StyledComicCard>
  )
}
