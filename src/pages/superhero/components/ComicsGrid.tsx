import { FC, Fragment, useCallback, useEffect, useRef, useState } from 'react'
import styled from 'styled-components'

import { Grid } from '../../../components/atoms/grid/Grid'
import { GridItem } from '../../../components/atoms/grid/GridItem'
import { Modal } from '../../../components/atoms/Modal'
import { Typography } from '../../../components/atoms/Typography'
import { useSuperHeroes } from '../../../contexts/superHeroContext/SuperHeroProvider'
import { Comic } from '../../../lib/interfaces/Comic'
import { ComicCard } from './ComicCard'

interface ComicsGridProps extends React.HTMLAttributes<HTMLDivElement> {
  superHeroName: string
}

const StyledComicsGrid = styled.section.attrs((props: ComicsGridProps) => ({}))``

export const ComicsGrid: FC<ComicsGridProps> = props => {
  /*
   * State
   */
  const { selectComics } = useSuperHeroes()
  const { superHeroName } = props

  const [comics, setComics] = useState(selectComics(superHeroName))

  const [selectedComic, setSelectedComic] = useState(false)
  const lastSelectedComic = useRef<Comic>()

  /*
   * Hooks
   */

  /*
   * Methods
   */
  const selectComic = useCallback(
    (comic: Comic) => () => {
      setSelectedComic(true)
      lastSelectedComic.current = comic
    },
    [setSelectedComic]
  )

  const deselectComic = useCallback(() => {
    setSelectedComic(false)
  }, [setSelectedComic])

  /*
   * Side effects
   */
  useEffect(() => {
    setComics(selectComics(superHeroName))
  }, [superHeroName, selectComics])

  /*
   * Render
   */
  return (
    <StyledComicsGrid {...props}>
      <Grid gap={24}>
        {comics.map(comic => (
          <GridItem key={comic.id} onClick={selectComic(comic)}>
            <ComicCard
              title={comic.title}
              creators={comic.creators}
              imageUri={comic.imageUri}
            />
          </GridItem>
        ))}
      </Grid>
      <Modal open={!!selectedComic} onClose={deselectComic}>
        {lastSelectedComic.current && (
          <Fragment>
            <Typography variant='h1' gutterBottom>
              {lastSelectedComic.current.title}
            </Typography>
            <Grid>
              <GridItem
                column={{
                  xs: '1/13',
                  sm: '1/2',
                  lg: '1/4',
                }}
              >
                <img
                  src={lastSelectedComic.current.imageUri}
                  alt={lastSelectedComic.current.title}
                />
              </GridItem>
              <GridItem
                column={{
                  xs: '1/13',
                  sm: '2/4',
                  lg: '4/7',
                }}
              >
                <Typography
                  dangerouslySetInnerHTML={{
                    __html: lastSelectedComic.current.description,
                  }}
                />
              </GridItem>
            </Grid>
          </Fragment>
        )}
      </Modal>
    </StyledComicsGrid>
  )
}
