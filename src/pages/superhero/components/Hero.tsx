import { FC } from 'react'
import styled from 'styled-components'

import { Typography } from '../../../components/atoms/Typography'
import { Avatar } from './Avatar'

interface HeroProps extends React.HTMLAttributes<HTMLDivElement> {
  title: string
  image: string
}

const StyledHero = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40vh;
  color: var(--color-light);
  text-transform: uppercase;
  position: relative;
  margin-bottom: 8rem;
  flex-direction: column;

  & > * {
    margin: 1rem;
  }

  & .hero-background {
    background-color: var(--color-dark);
    width: 110vw;
    position: absolute;
    margin-top: -15%;
    bottom: 0;
    top: 0;
    z-index: -1;
    transform: rotate(-4deg);
  }

  @media (min-width: 1024px) {
    flex-direction: row;
  }
`

export const Hero: FC<HeroProps> = props => {
  /**
   * State
   */
  const { title, image } = props

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /**
   * Render
   */
  return (
    <StyledHero {...props}>
      <div className='hero-background' />
      <Typography variant='h2'>{title}</Typography>
      <Avatar src={image} alt={title} />
    </StyledHero>
  )
}
