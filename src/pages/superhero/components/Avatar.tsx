import { FC } from 'react'
import styled from 'styled-components'

interface AvatarProps extends React.ImgHTMLAttributes<HTMLImageElement> {}

const StyledAvatar = styled.img.attrs((props: AvatarProps) => ({}))`
  width: 200px;
  height: 200px;
  border-radius: 50%;
`

export const Avatar: FC<AvatarProps> = props => {
  /*
   * State
   */

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  return <StyledAvatar {...props} />
}
