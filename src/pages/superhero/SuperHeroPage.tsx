import { FC, useEffect, useRef } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import styled from 'styled-components'

import { fetchComics } from '../../api/fetchComics'
import { Container } from '../../components/atoms/Container'
import { Divider } from '../../components/atoms/Divider'
import { Typography } from '../../components/atoms/Typography'
import { superHeroLoadingData } from '../../contexts/superHeroContext/superHeroActions'
import { useSuperHeroes } from '../../contexts/superHeroContext/SuperHeroProvider'
import { SuperHero } from '../../lib/interfaces/SuperHero'
import { ComicsGrid } from './components/ComicsGrid'
import { Hero } from './components/Hero'

interface SuperHeroPageProps extends React.HTMLAttributes<HTMLDivElement> {}

const StyledSuperHeroPage = styled.section.attrs((props: SuperHeroPageProps) => ({}))``

export const SuperHeroPage: FC<SuperHeroPageProps> = props => {
  /*
   * State
   */
  const { id } = useParams()

  const { selectSuperHero, dispatch } = useSuperHeroes()

  const superHero = useRef<SuperHero | undefined>(selectSuperHero(Number(id)))

  /*
   * Hooks
   */
  const navigate = useNavigate()

  /*
   * Methods
   */

  /*
   * Side effects
   */
  // Make sure we have a super hero
  // TODO: fetch superhero by id if we do not have one
  useEffect(() => {
    if (superHero.current !== undefined) return

    navigate('/')
  }, [navigate])

  // Fetch the comics of the super hero
  useEffect(() => {
    const idAsNumber = Number(id)
    if (isNaN(idAsNumber)) return

    dispatch([fetchComics(idAsNumber), superHeroLoadingData(true)])
  }, [id, dispatch])

  /*
   * Render
   */
  if (!superHero.current) return null

  return (
    <StyledSuperHeroPage>
      <Hero title={superHero.current.name} image={superHero.current.imageUri} />
      <Container>
        <Divider disabled />
        <Typography variant='h3' style={{ margin: '2rem 0' }}>
          COMICS
        </Typography>
        <ComicsGrid superHeroName={superHero.current.name} />
      </Container>
    </StyledSuperHeroPage>
  )
}
