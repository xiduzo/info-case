import { FC, useCallback } from 'react'
import { useNavigate } from 'react-router-dom'

import { Grid } from '../../../components/atoms/grid/Grid'
import { GridItem } from '../../../components/atoms/grid/GridItem'
import { useSuperHeroes } from '../../../contexts/superHeroContext/SuperHeroProvider'
import { SuperHeroCard } from './SuperHeroCard'

interface SuperHeroGridProps extends React.HTMLAttributes<HTMLDivElement> {
  filterByName?: string
}

export const SuperHeroGrid: FC<SuperHeroGridProps> = props => {
  /**
   * State
   */
  const { filterByName } = props
  const { superHeroes } = useSuperHeroes()

  /**
   * Hooks
   */
  const navigate = useNavigate()

  /**
   * Methods
   */
  const gotoSuperHero = useCallback(
    (id: number) => (): void => {
      navigate(`/superhero/${id}`)
    },
    [navigate]
  )

  /**
   * Side effects
   */
  return (
    <Grid {...props}>
      {superHeroes
        .filter(superHero => {
          if (!filterByName) return true
          return superHero.name.toLowerCase().includes(filterByName.toLowerCase())
        })
        .sort((a, b) => a.name.localeCompare(b.name))
        .map(superHero => (
          <GridItem key={superHero.id} onClick={gotoSuperHero(superHero.id)}>
            <SuperHeroCard superHero={superHero} />
          </GridItem>
        ))}
    </Grid>
  )
}
