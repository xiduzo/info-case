import { FC, useCallback, useEffect } from 'react'
import styled from 'styled-components'

import { fetchSuperHeroes } from '../../../api/fetchSuperHeroes'
import { Button } from '../../../components/atoms/buttons/Button'
import { superHeroLoadingData } from '../../../contexts/superHeroContext/superHeroActions'
import { useSuperHeroes } from '../../../contexts/superHeroContext/SuperHeroProvider'

interface SuperHeroLoaderProps extends React.HTMLAttributes<HTMLDivElement> {}

const StyledSuperHeroLoader = styled.div.attrs((props: SuperHeroLoaderProps) => ({}))`
  display: flex;
  justify-content: center;
`

export const SuperHeroLoader: FC<SuperHeroLoaderProps> = props => {
  /*
   * State
   */
  const { offset, dispatch } = useSuperHeroes()

  /*
   * Hooks
   */

  /*
   * Methods
   */
  const loadMoreHeroes = useCallback(() => {
    dispatch([superHeroLoadingData(true), fetchSuperHeroes({ offset })])
  }, [offset, dispatch])

  /*
   * Side effects
   */
  useEffect(() => {
    if (offset > 0) return // We already have fetched some data before
    dispatch([superHeroLoadingData(true), fetchSuperHeroes({ offset: 0 })])
  }, [dispatch, offset])

  /*
   * Render
   */
  return (
    <StyledSuperHeroLoader {...props}>
      <Button onClick={loadMoreHeroes}>Load more</Button>
    </StyledSuperHeroLoader>
  )
}
