import { FC, memo } from 'react'
import styled from 'styled-components'

import { ButtonBase } from '../../../components/atoms/buttons/ButtonsBase'
import { Divider } from '../../../components/atoms/Divider'
import { Typography } from '../../../components/atoms/Typography'
import { SuperHero } from '../../../lib/interfaces/SuperHero'

interface SuperHeroCardProps extends React.HTMLAttributes<HTMLDivElement> {
  superHero: SuperHero
}

const StyledSuperHeroCard = styled(ButtonBase).attrs((props: SuperHeroCardProps) => ({
  image: props.superHero.imageUri,
}))`
  color: white;
  position: relative;
  overflow: hidden;

  & > article {
    background: var(--color-secondary);
    padding: 8px 16px;
    display: flex;
    min-height: 125px;
    justify-content: space-between;
    flex-direction: column;
    position: relative;

    & .typography {
      z-index: 1;
    }

    & > .hover-backdrop {
      background: var(--color-primary);
      width: 100%;
      position: absolute;
      left: 0;
      top: 0;
      bottom: 100%;
      transition: all 0.3s ease-out;
    }
  }

  & > figure {
    height: 250px;
    position: relative;
    overflow: hidden;

    > img {
      height: 100%;
      width: 100%;
      background-color: var(--color-secondary);
      transition: all 0.3s ease-out;
    }
  }

  &:hover {
    & > article > .hover-backdrop {
      transition: all 0.1s ease-in;
      bottom: 0;
    }

    & > figure > img {
      transform: scale(1.1) rotate(1deg);
      transition: all 0.1s ease-in;
    }
  }

  &:after {
    content: '';
    position: absolute;
    bottom: -1px;
    right: -1px;
    border-top: 16px solid transparent;
    border-right: 16px solid white;
    height: 0;
  }
`

export const SuperHeroCard: FC<SuperHeroCardProps> = memo(props => {
  /**
   * State
   */
  const { superHero } = props

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  return (
    <StyledSuperHeroCard {...props}>
      <figure>
        <img src={superHero.imageUri} alt={superHero.name} />
      </figure>
      <Divider color='primary' />
      <article>
        {/* hover backdrop */}
        <div className='hover-backdrop' />
        <Typography variant='h2'>{superHero.name}</Typography>
        <Typography variant='caption'>{superHero.comics.available} comics</Typography>
      </article>
    </StyledSuperHeroCard>
  )
})
