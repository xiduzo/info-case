import { ChangeEvent, FC, useEffect, useRef, useState } from 'react'
import styled from 'styled-components'

import { fetchSuperHeroes } from '../../../api/fetchSuperHeroes'
import { Divider } from '../../../components/atoms/Divider'
import { SearchIcon } from '../../../components/atoms/icons/SearchIcon'
import { superHeroLoadingData } from '../../../contexts/superHeroContext/superHeroActions'
import { useSuperHeroes } from '../../../contexts/superHeroContext/SuperHeroProvider'
import { useDebounce } from '../../../hooks/useDebounce'

interface SearchBarProps extends React.HTMLAttributes<HTMLDivElement> {
  onSearch?: (searchTerm: string) => void
}

const StyledSearchBar = styled.div.attrs((props: SearchBarProps) => ({}))`
  > div {
    display: flex;
    align-items: center;
    margin-bottom: 0.25rem;
  }

  input {
    width: 100%;
    border: none;
    font-size: 1.5rem;
  }
`

export const SearchBar: FC<SearchBarProps> = props => {
  /*
   * State
   */
  const { onSearch } = props

  const [searchTerm, setSearchTerm] = useState('')
  const inputRef = useRef<HTMLInputElement | null>(null)

  /*
   * Hooks
   */
  const { dispatch } = useSuperHeroes()

  /*
   * Methods
   */
  const inputHandler = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value)
  }

  /*
   * Side effects
   */
  useDebounce(searchTerm, 300, searchFor => {
    if (onSearch) onSearch(searchFor)

    if (searchFor === '') return

    dispatch([
      superHeroLoadingData(true),
      fetchSuperHeroes({ nameStartsWith: searchFor }),
    ])
  })

  useEffect(() => {
    setTimeout(() => {
      if (!inputRef.current) return
      inputRef.current.focus()
    }, 0) // Hacking the render cycle to focus the input after the component is mounted
  }, [])

  /*
   * Render
   */
  return (
    <StyledSearchBar {...props}>
      <div>
        <SearchIcon />
        <input
          ref={inputRef}
          placeholder='Search character'
          value={searchTerm}
          onChange={inputHandler}
        />
      </div>
      <Divider />
    </StyledSearchBar>
  )
}
