import { FC, useState } from 'react'

import { Container } from '../../components/atoms/Container'
import { SearchBar } from './components/SearchBar'
import { SuperHeroGrid } from './components/SuperHeroGrid'
import { SuperHeroLoader } from './components/SuperHeroLoader'

export const HomePage: FC = () => {
  const [searchTerm, setSearchTerm] = useState<string>()

  return (
    <Container>
      <SearchBar onSearch={setSearchTerm} />
      <SuperHeroGrid style={{ margin: '2rem 0' }} filterByName={searchTerm} />
      <SuperHeroLoader />
    </Container>
  )
}
