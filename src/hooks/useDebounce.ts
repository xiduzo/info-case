import { useEffect, useRef } from 'react'

type DebounceCallback<T> = (newValue: T, previousValue: T) => void

export const useDebounce = <T>(
  value: T,
  debounceTime: number,
  onDebounce: DebounceCallback<T>
) => {
  const initialValue = useRef(value)

  useEffect(() => {
    const handler = setTimeout(() => {
      if (initialValue.current === value) return

      onDebounce && onDebounce(value, initialValue.current)
      initialValue.current = value
    }, debounceTime)

    return () => {
      clearTimeout(handler)
    }
  }, [value, debounceTime, onDebounce])
}
