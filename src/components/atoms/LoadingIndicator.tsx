import { FC } from 'react'
import styled from 'styled-components'

import { useSuperHeroes } from '../../contexts/superHeroContext/SuperHeroProvider'

interface LoadingIndicatorProps extends React.HTMLAttributes<HTMLDivElement> {}

const StyledLoadingIndicator = styled.div.attrs((props: LoadingIndicatorProps) => ({}))`
  z-index: 1000;
`

export const LoadingIndicator: FC<LoadingIndicatorProps> = props => {
  /*
   * State
   */
  const { loading } = useSuperHeroes()

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  if (!loading) return null

  return (
    <StyledLoadingIndicator
      {...props}
      className={`loading ${props.className ?? ''}`.trim()}
    ></StyledLoadingIndicator>
  )
}
