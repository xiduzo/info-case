import { FC } from 'react'
import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'

import logo from '../../illustrations/logo.svg'

const StyledHeader = styled.header`
  background-color: var(--color-secondary);
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1rem;

  & > img {
    width: 100px;
  }
`

export const AppBar: FC = () => {
  /**
   * State
   */

  /**
   * Hooks
   */
  const navigate = useNavigate()

  /**
   * Methods
   */
  const goToHome = () => navigate('/')

  /*
   * Side effects
   */

  /*
   * Render
   */
  return (
    <StyledHeader>
      <img src={logo} alt='Marvel logo' onClick={goToHome} />
    </StyledHeader>
  )
}
