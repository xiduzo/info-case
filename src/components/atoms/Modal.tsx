import { FC, useEffect } from 'react'
import styled from 'styled-components'

import { disableScroll, enabledScroll } from '../../utils/scroll'
import { CloseIcon } from './icons/CloseIcon'

interface ModalProps extends React.HTMLAttributes<HTMLDivElement> {
  open?: boolean
  onClose?: () => void
}

const StyledModal = styled.div.attrs((props: ModalProps) => ({
  open: props.open ?? false,
}))`
  display: flex;
  justify-content: center;
  align-items: center;

  > div {
    position: fixed;
    top: 0;
    left: 0;
    width: ${props => (props.open ? '100vw' : '0')};
    height: ${props => (props.open ? '100vh' : '0')};
    background-color: rgba(0, 0, 0, 0.33);
    opacity: ${props => (props.open ? 1 : 0)};
    transition: opacity 0.3s ease-in-out;
  }

  > section {
    background: var(--color-light);
    padding: 32px;
    position: fixed;
    top: 15vh;
    opacity: ${props => (props.open ? 1 : 0)};
    transform: ${props => (props.open ? 'translateY(0)' : 'translateY(-5vh)')};
    max-width: 800px;
    min-width: 30vw;
    transition: all ${props => (props.open ? '0.2s' : '0s')} ease-in-out;
    transition-delay: ${props => (props.open ? '0.1s' : '0s')};
    transition-property: opacity, transform;
    z-index: ${props => (props.open ? 1 : -1)};
    margin: 0 1rem;

    @media (max-width: 768px) {
      top: 5vh;
      padding: 16px;
    }

    & .close-handler {
      display: flex;
      justify-content: flex-end;
      margin-bottom: 1rem;
    }
  }
`

export const Modal: FC<ModalProps> = props => {
  /*
   * State
   */
  const { open, onClose, children } = props

  /*
   * Hooks
   */

  /*
   * Methods
   */
  const closeModal = (): void => {
    if (onClose) onClose()
  }

  /*
   * Side effects
   */
  useEffect(() => {
    if (open) {
      disableScroll()
      return
    }

    enabledScroll()

    return enabledScroll
  }, [open])

  /*
   * Render
   */
  return (
    <StyledModal {...props}>
      <div onClick={closeModal} />
      <section>
        <div className='close-handler'>
          <CloseIcon onClick={closeModal} />
        </div>
        {children}
      </section>
    </StyledModal>
  )
}
