import { FC } from 'react'
import styled from 'styled-components'

import { ButtonBase } from './ButtonsBase'

interface ButtonProps extends React.HTMLAttributes<HTMLDivElement> {}

const StyledButton = styled(ButtonBase)`
  background-color: var(--color-primary);
  border: none;
  color: white;
  padding: 1rem 2rem;
  text-transform: uppercase;
  font-weight: 700;
  position: relative;

  &:before {
    content: '';
    position: absolute;
    top: -1px;
    left: -1px;
    border-bottom: 16px solid transparent;
    border-left: 16px solid white;
    height: 0;
  }

  &:after {
    content: '';
    position: absolute;
    bottom: -1px;
    right: -1px;
    border-top: 16px solid transparent;
    border-right: 16px solid white;
    height: 0;
  }
`

export const Button: FC<ButtonProps> = props => {
  /**
   * State
   */
  const { children } = props

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  return <StyledButton {...props}>{children}</StyledButton>
}
