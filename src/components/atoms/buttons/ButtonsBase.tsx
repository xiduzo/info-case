import { FC } from 'react'
import styled from 'styled-components'

interface ButtonBaseProps {}

const StyleButtonBase = styled.div`
  display: inline-block;
  &:hover {
    cursor: pointer;
  }
`

export const ButtonBase: FC<ButtonBaseProps> = props => {
  /**
   * State
   */
  const { children } = props

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  return <StyleButtonBase {...props}>{children}</StyleButtonBase>
}
