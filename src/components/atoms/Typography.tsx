import { FC } from 'react'
import styled from 'styled-components'

type FontVariant = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'body' | 'caption'

const fontStyle = (variant: FontVariant = 'body'): { size: string; weight: string } => {
  switch (variant) {
    case 'h1':
      return {
        size: '1.75rem',
        weight: 'bold',
      }
    case 'h2':
      return {
        size: '1.5rem',
        weight: 'bold',
      }
    case 'h3':
      return {
        size: '1.75rem',
        weight: '400',
      }
    case 'h4':
      return {
        size: '1rem',
        weight: '700',
      }
    case 'caption':
      return {
        size: '1rem',
        weight: '100',
      }
    default:
      return {
        size: '1rem',
        weight: '400',
      }
  }
}

interface TypographyProps extends React.HTMLAttributes<HTMLDivElement> {
  variant?: FontVariant
  gutterBottom?: boolean
}

const StyledTypography = styled.div.attrs((props: TypographyProps) => ({
  variant: props.variant,
  gutterBottom: props.gutterBottom,
}))`
  font-size: ${(props: TypographyProps) => fontStyle(props.variant).size};
  font-weight: ${(props: TypographyProps) => fontStyle(props.variant).weight};
  opacity: ${(props: TypographyProps) => (props.variant === 'caption' ? '0.7' : '1')};
  margin-bottom: ${(props: TypographyProps) => (props.gutterBottom ? '1rem' : '0')};
`

export const Typography: FC<TypographyProps> = props => {
  /**
   * State
   */
  const { children, className } = props

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  return (
    <StyledTypography
      {...props}
      className={`typography ${props.className ?? ''} ${className ?? ''}`.trim()}
    >
      {children}
    </StyledTypography>
  )
}
