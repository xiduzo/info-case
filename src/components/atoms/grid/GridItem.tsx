import { FC } from 'react'
import styled from 'styled-components'

// TODO: add better typing => '1/2' | '1/3' | '1/4' etc
type Col = string

interface GridItemProps extends React.HTMLAttributes<HTMLDivElement> {
  column?: {
    xs?: Col
    sm?: Col
    md?: Col
    lg?: Col
  }
}

const StyledGridItem = styled.div.attrs((props: GridItemProps) => ({
  column: {
    xs: undefined,
    sm: undefined,
    md: undefined,
    lg: undefined,
    ...props.column,
  },
}))`
  grid-column: ${props => props.column?.xs};

  @media (min-width: 768px) {
    grid-column: ${props => props.column?.sm ?? props.column?.xs};
  }

  @media (min-width: 1024px) {
    grid-column: ${props => props.column?.md ?? props.column?.sm ?? props.column?.xs};
  }

  @media (min-width: 1201px) {
    grid-column: ${props =>
      props.column?.lg ?? props.column?.md ?? props.column?.sm ?? props.column?.xs};
  }

  & > * {
    width: 100%;
    height: 100%;
  }
`

export const GridItem: FC<GridItemProps> = props => {
  /**
   * State
   */
  const { children } = props

  /**
   * Hooks
   */

  /**
   * Methods
   */

  /**
   * Side effects
   */

  /**
   * Render
   */
  // @ts-ignore
  return <StyledGridItem {...props}>{children}</StyledGridItem>
}
