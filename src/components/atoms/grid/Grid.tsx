import { FC } from 'react'
import styled from 'styled-components'

type Cols = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12

interface GridProps extends React.HTMLAttributes<HTMLDivElement> {
  columns?: {
    xs?: Cols
    sm?: Cols
    md?: Cols
    lg?: Cols
  }
  gap?: number
}

const columns = 12

const StyledGrid = styled.div.attrs((props: GridProps) => ({
  columns: {
    xs: 'maxCols',
    sm: 6,
    md: 4,
    lg: 2,
    ...props.columns,
  },
  gap: props.gap ? `${props.gap}px` : '1rem',
}))`
  display: grid;
  grid-template-columns: repeat(
    ${props => columns / Number(props.columns?.xs ?? columns)},
    1fr
  );
  gap: ${props => props.gap};

  @media (min-width: 768px) {
    grid-template-columns: repeat(
      ${props => columns / Number(props.columns?.sm ?? 6)},
      1fr
    );
  }

  @media (min-width: 1024px) {
    grid-template-columns: repeat(
      ${props => columns / Number(props.columns?.md ?? 3)},
      1fr
    );
  }

  @media (min-width: 1201px) {
    grid-template-columns: repeat(
      ${props => columns / Number(props.columns?.lg ?? 2)},
      1fr
    );
  }
`

export const Grid: FC<GridProps> = props => {
  /**
   * State
   */
  const { children } = props

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  return <StyledGrid {...props}>{children}</StyledGrid>
}
