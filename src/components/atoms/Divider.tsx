import { FC } from 'react'
import styled from 'styled-components'

interface DividerProps extends React.HTMLAttributes<HTMLDivElement> {
  disabled?: boolean
  color?: 'primary'
}

const StyledDivider = styled.hr.attrs((props: DividerProps) => ({
  disabled: props.disabled,
}))`
  height: 4px;
  background: ${props =>
    props.color === 'primary' ? 'var(--color-primary)' : 'var(--color-secondary)'};
  border: none;
  margin: 0;
  opacity: ${props => (props.disabled ? 0.3 : 1)};
`

export const Divider: FC<DividerProps> = props => <StyledDivider {...props} />
