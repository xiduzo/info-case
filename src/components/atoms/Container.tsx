import { FC } from 'react'
import styled from 'styled-components'

interface ContainerProps extends React.HTMLAttributes<HTMLDivElement> {
  maxWidth?: number
}

const StyledContainer = styled.section.attrs((props: ContainerProps) => ({
  maxWidth: props.maxWidth ?? 1600,
}))`
  max-width: ${props => props.maxWidth}px;
  margin: 0 auto;
  padding: 2rem 1rem;
`

export const Container: FC<ContainerProps> = props => {
  /*
   * State
   */
  const { children } = props

  /*
   * Hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Render
   */
  return <StyledContainer {...props}>{children}</StyledContainer>
}
