importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.2.0/workbox-sw.js')
/* eslint-disable no-undef */

if (workbox) {
  console.log(`Workbox is loaded 🎉`)
} else {
  console.log(`Workbox didn't load `)
}

// eslint-disable-next-line
workbox.precaching.precacheAndRoute(self.__WB_MANIFEST)

// // eslint-disable-next-line
self.addEventListener('install', event => event.waitUntil(self.skipWaiting()))
// // eslint-disable-next-line
self.addEventListener('activate', event => event.waitUntil(self.clients.claim()))

// // //Cache cdn files and external links
workbox.routing.registerRoute(
  new RegExp('https:.*.(css|js|json|)'),
  new workbox.strategies.NetworkFirst({ cacheName: 'external-cache' })
)

workbox.routing.registerRoute(
  ({ request }) => request.destination === 'image',
  new workbox.strategies.StaleWhileRevalidate()
)
