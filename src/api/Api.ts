import md5 from 'md5'

export const PUBLIC_KEY = process.env.REACT_APP_PUBLIC_KEY ?? 'not_set'
export const PRIVATE_KEY = process.env.REACT_APP_PRIVATE_KEY ?? 'not_set'

const generateHash = (timeStamp: number): string =>
  md5(`${timeStamp}${PRIVATE_KEY}${PUBLIC_KEY}`)

// https://developer.marvel.com/documentation/authorization
const urlWithToken = (url: string): string => {
  const timeStamp = performance.now()
  const hash = generateHash(timeStamp)

  return `${url}?apikey=${PUBLIC_KEY}&ts=${timeStamp}&hash=${hash}`
}

const urlWithParams = (
  url: string,
  queryParams?: Record<string, string | number | undefined>
): string => {
  const paramsToString = Object.entries(queryParams ?? {}).reduce(
    (urlWithParams, param) =>
      param[1] ? (urlWithParams += `&${param[0]}=${param[1]}`) : urlWithParams,
    ''
  )

  return `${url}${paramsToString}`
}

export const Api = {
  fetch: async (
    url: string,
    queryParams?: Record<string, string | number | undefined>
  ) => await fetch(urlWithParams(urlWithToken(url), queryParams)),
}
