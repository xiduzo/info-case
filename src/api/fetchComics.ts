import { addComics, superHeroLoadingData } from '../contexts/superHeroContext/superHeroActions'
import { BASE_URL } from '../lib/constants/marvelApiConstants'
import { HttpStatusCode } from '../lib/enums/HttpStatusCode'
import { Thunk } from '../lib/types/Thunk'
import { comicMapper } from '../utils/mappers'
import { Api } from './Api'

export const fetchComics =
  (superHeroId: number, options?: { offset?: number }): Thunk =>
  async dispatch => {
    const request = await Api.fetch(`${BASE_URL}/characters/${superHeroId}/comics`, {
      offset: 0,
      ...options,
    })
    const response = await request.json()

    if (response.code === HttpStatusCode.OK) {
      const comics = response.data.results.map(comicMapper)
      dispatch(addComics(comics))

      if (response.data.total > response.data.offset + response.data.count) {
        dispatch([
          superHeroLoadingData(true),
          fetchComics(superHeroId, {
            offset: response.data.offset + response.data.count,
          }),
        ])
      }
    }

    // TODO: error handling
  }
