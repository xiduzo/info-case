import { addHeroes } from '../contexts/superHeroContext/superHeroActions'
import { BASE_URL } from '../lib/constants/marvelApiConstants'
import { HttpStatusCode } from '../lib/enums/HttpStatusCode'
import { Thunk } from '../lib/types/Thunk'
import { superHeroMapper } from '../utils/mappers'
import { Api } from './Api'

export const fetchSuperHeroes =
  (options?: { offset?: number; nameStartsWith?: string }): Thunk =>
  async dispatch => {
    const request = await Api.fetch(`${BASE_URL}/characters`, options)
    const response = await request.json()

    if (response.code === HttpStatusCode.OK) {
      const heroes = response.data.results.map(superHeroMapper)
      dispatch(
        addHeroes(
          heroes,
          options?.offset !== undefined ? options.offset + heroes.length : 0
        )
      )
    }

    // TODO: error handling
  }
