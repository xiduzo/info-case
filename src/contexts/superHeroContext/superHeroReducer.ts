import { LocalStorageKey } from '../../lib/enums/LocalStorageKey'
import { setLocalItem } from '../../utils/offlineManager'
import { SuperHeroAction } from './superHeroActions'
import { SuperHeroContextState } from './SuperHeroProvider'

export const superHeroesReducer = (
  state: SuperHeroContextState,
  action: SuperHeroAction
) => {
  switch (action.type) {
    case 'add-heroes':
      return {
        ...state,
        superHeroes: setLocalItem(LocalStorageKey.SuperHeroes, [
          ...state.superHeroes.filter(superHero => {
            if (action.superHeroes.some(hero => hero.id === superHero.id)) {
              return false
            }
            return superHero
          }),
          ...action.superHeroes,
        ]),
        offset: setLocalItem(LocalStorageKey.Offset, action.offset ?? state.offset),
        loading: false,
      }
    case 'add-comics':
      return {
        ...state,
        comics: setLocalItem(LocalStorageKey.Comics, [
          ...state.comics.filter(comic => {
            if (action.comics.some(actionComic => actionComic.id === comic.id)) {
              return false
            }
            return comic
          }),
          ...action.comics,
        ]),
        loading: false,
      }
    case 'loading-data':
      return {
        ...state,
        loading: action.isLoading,
      }
    default:
      return state
  }
}
