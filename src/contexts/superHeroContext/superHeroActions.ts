import { Comic } from '../../lib/interfaces/Comic'
import { SuperHero } from '../../lib/interfaces/SuperHero'

type AddHeroes = {
  type: 'add-heroes'
  superHeroes: SuperHero[]
  offset?: number
}

type AddComics = {
  type: 'add-comics'
  comics: Comic[]
}

type LoadingData = {
  type: 'loading-data'
  isLoading: boolean
}

export const addHeroes = (superHeroes: SuperHero[], offset?: number): AddHeroes => ({
  type: 'add-heroes',
  superHeroes,
  offset,
})

export const superHeroLoadingData = (isLoading: boolean): LoadingData => ({
  type: 'loading-data',
  isLoading,
})

export const addComics = (comics: Comic[]): AddComics => ({
  type: 'add-comics',
  comics,
})

export type SuperHeroAction = AddHeroes | LoadingData | AddComics
