import { createContext, FC, useCallback, useContext, useMemo, useReducer } from 'react'

import { LocalStorageKey } from '../../lib/enums/LocalStorageKey'
import { Comic } from '../../lib/interfaces/Comic'
import { SuperHero } from '../../lib/interfaces/SuperHero'
import { getLocalItem } from '../../utils/offlineManager'
import { SuperHeroAction } from './superHeroActions'
import { superHeroesReducer } from './superHeroReducer'

export interface SuperHeroContextState {
  /**
   * Fetched super heroes
   */
  superHeroes: SuperHero[]
  /**
   * Fetched comics
   */
  comics: Comic[]
  /**
   * Is currently fetching / loading
   */
  loading: boolean
  /**
   * Last super hero offset which has been fetched
   */
  offset: number
  /**
   * A method to dispatch actions into the provider
   * Is able to both dispatch reducer actions or thunks
   */
  dispatch: (action: unknown) => void
  /**
   * Selection of a single super hero
   */
  selectSuperHero: (superHeroId: number) => SuperHero | undefined
  /**
   * Selection of all comics related to a super hero
   */
  selectComics: (superHeroName: string) => Comic[]
}

const initialState: SuperHeroContextState = {
  superHeroes: getLocalItem(LocalStorageKey.SuperHeroes, []),
  comics: getLocalItem(LocalStorageKey.Comics, []),
  loading: false,
  offset: getLocalItem(LocalStorageKey.Offset, 0),
  dispatch: (action: unknown) => alert('not implemented'),
  selectSuperHero: (superHeroId: number) => undefined,
  selectComics: (superHeroName: string) => [],
}

const SuperHeroContext = createContext(initialState)

export const SuperHeroProvider: FC = props => {
  const [state, reducerDispatch] = useReducer(superHeroesReducer, initialState)

  const superHeroes = useMemo(() => state.superHeroes, [state.superHeroes])
  const loading = useMemo(() => state.loading, [state.loading])
  const comics = useMemo(() => state.comics, [state.comics])
  const offset = useMemo(() => state.offset, [state.offset])

  const selectSuperHero = useCallback(
    (superHeroId: number) =>
      superHeroes.find(superHero => superHero.id === superHeroId),
    [superHeroes]
  )

  const selectComics = useCallback(
    (superHeroName: string) =>
      comics.filter(comic => comic.superHeroes.includes(superHeroName)),
    [comics]
  )

  const dispatch = useCallback(
    (action: unknown): void => {
      // Poor mans thunks
      if (typeof action === 'function') {
        action(dispatch) // Dispatch of a thunk
        return
      }

      // Enable dispatches of arrays
      if (Array.isArray(action)) {
        action.forEach(dispatch)
        return
      }

      const superHeroAction = action as SuperHeroAction
      if (!superHeroAction.type) return

      reducerDispatch(superHeroAction)
    },
    [reducerDispatch]
  )

  return (
    <SuperHeroContext.Provider
      value={{
        superHeroes,
        offset,
        comics,
        loading,
        selectSuperHero,
        selectComics,
        dispatch,
      }}
    >
      {props.children}
    </SuperHeroContext.Provider>
  )
}

export const useSuperHeroes = () => {
  const context = useContext(SuperHeroContext)

  if (!context) throw new Error('useSuperHeroes must be used within SuperHeroContext')

  return context
}
